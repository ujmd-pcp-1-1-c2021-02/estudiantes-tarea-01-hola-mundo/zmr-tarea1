﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HolaMundo.last
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public object DynamicLinkLabel { get; private set; }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = ("Buenos dias alegria, buenos dias señor sol");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = ("Adios mundo cruel");
        }

        private void label1_Click(object sender, EventArgs e)
        {
            
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=CzoxmeoSdSs");
        }
    }
}
